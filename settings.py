# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""
import os

PROXY_LIST = None

USE_CACHE = False
MONGO_DB = 'merlion'

DEBUG = True
THREADS = 8
