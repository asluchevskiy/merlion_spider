# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""
# -*- coding: utf-8 -*-
import re
import logging
import signal
import string
import json
import pymongo
from datetime import datetime
from pyquery import PyQuery
from grab.spider import Spider, Task
from grab.tools.text import find_number, normalize_space
from grab.tools.captcha import CaptchaSolver
from urllib import urlencode
from urlparse import urlsplit
from time import sleep
try:
    import local_settings as settings
except ImportError:
    import settings

class BaseSpider(Spider):

    def shutdown(self):
        print self.render_stats()

    def sigint_handler(self, signal, frame):
        logging.info('Interrupting...')
        self.should_stop = True

    def prepare(self):
        signal.signal(signal.SIGINT, self.sigint_handler)
        if settings.PROXY_LIST:
            self.load_proxylist(settings.PROXY_LIST, 'text_file', auto_change=True)
        if settings.USE_CACHE:
            self.setup_cache(database=settings.MONGO_DB)
        self.db = pymongo.Connection()[settings.MONGO_DB]

    def shutdown(self):
        pass

class MerlionSpider(BaseSpider):

    initial_urls = ['http://www.merlion.ru/goods/catalog/cat/']

    def task_initial(self, grab, task):
        pq = grab.pyquery
        div_list = pq.find('div.catalog_link')
#        yield Task('list', 'http://www.merlion.ru/goods/catalog/cat/n100/n10008/p2/', page=2, path=[])
#        return
        for i in range(len(div_list)):
            path0 = div_list.eq(i).find('h3').text()
            ul_list = div_list.eq(i).find('ul.catalog_list')
            for j in range(len(ul_list)):
                path1 = ul_list.eq(j).find('a[href="#"]').text()
                li_list = ul_list.eq(j).find('ul li')
                for k in range(len(li_list)):
                    path2 = li_list.eq(k).text()
                    path = [path0, path1, path2]
                    href = li_list.eq(k).find('a').attr('href')
                    yield Task('list', grab.make_url_absolute(href), path=path, page=1)

    def task_list(self, grab, task):
        pq = grab.pyquery
        if task.page == 1:
            for a in pq.find('div[align="left"]').eq(0).find('a')[1:]:
                yield Task('list', grab.make_url_absolute(a.get('href')), path=task.path, page=int(a.text))
        i_list = pq.find('td[width="650px"]').children('table').find('tr[valign="top"]')
        for i in range(len(i_list)):
            article = find_number(i_list.eq(i).find('div div').eq(0).text())
            brand = i_list.eq(i).find('div div').eq(0).find('strong').text()
            a = i_list.eq(i).find('div div').eq(1).find('a').eq(0)
            name = a.text()
            url = a.attr('href')
            warranty = i_list.eq(i).find('div.text')[0].text.strip(' | ').replace(u'Гарантия: ', '')
            if not warranty:
                warranty = None
            data = dict(_id=article, brand=brand, path=task.path, name=name, url=url, warranty=warranty)
            item = self.db.item.find_one(dict(_id=article))
            if not item:
                item = data
            if not item.get('is_parsed'):# or not item.get('is_images_downloaded'):
                yield Task('page', url, data=data)

    def task_page(self, grab, task):
        pq = grab.pyquery
        attributes = []
        trs = pq.find('table.text').eq(1).children('tr')
        for i in xrange(len(trs)):
            tr = trs.eq(i)
            if tr.find('td').length == 2:
                d = {}
                if tr.find('td').eq(0).find('a'):
                    d['key'] = tr.find('td').eq(0).find('a').text()
                else:
                    d['key'] = tr.find('td').eq(0).text()
                raw_value = tr.find('td').eq(1).html()
                raw_value = raw_value.replace('<br>', '\n') if raw_value else u''
                d['value'] = re.sub('<.+?>','',raw_value)
                if tr.find('td').eq(1).find('a'):
                    search = re.search('.+GoURL\(\'(http:\/\/[\w\d\._-]+)\'', tr.find('td').eq(1).html())
                    if search:
                        d['url'] = search.group(1)
                attributes.append(d)
        if not attributes:
            attributes = None
        # картинки
        images = []
        for a in pq.find('td.cat-r1 a'):
            images.append(a.get('href'))
        if not images:
            html = pq.find('table.text').eq(0).children('tr').children('td').eq(1).find('a').eq(0).attr('href')
            if html:
                search = re.search('^.+ShowPhoto\(\'(http:\/\/[\w\d\./_]+)\'', html )
                if search:
                    images.append(search.group(1))
        if not images:
            i = pq.find('table.text').eq(0).children('tr').children('td').eq(1).find('img').attr('src')
            if i:
                images.append( i )
        images_res = []
        search = re.compile('^http:\/\/[\w.\/]+\/(\d+_[\d\w]+_[smb]\.jpg)$')
        for i in images:
            s = search.search(i)
            if s:
                images_res.append(s.group(1))
        if not images_res:
            images_res = None

        task.data.update(dict(attributes=attributes, images=images_res, is_parsed=True))
        self.db.item.save(task.data)
