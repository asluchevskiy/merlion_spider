# -*- coding: utf-8 -*-
import os
import sys
import logging
import locale
import optparse
try:
    import local_settings as settings
except ImportError:
    import settings
from MerlionSpider import MerlionSpider

if __name__ == '__main__':
    # hack for unicode console i/o
    reload(sys)
    sys.setdefaultencoding(locale.getpreferredencoding())
    if settings.DEBUG:
        logging.basicConfig(level=logging.DEBUG)

    parser = optparse.OptionParser("usage: %prog [options]")
#    parser.add_option('-m', '--mode', action="store", dest="mode", default='search',
#        help='mode to run: search|pages')
    (options, args) = parser.parse_args()

    MerlionSpider(thread_number=settings.THREADS).run()
